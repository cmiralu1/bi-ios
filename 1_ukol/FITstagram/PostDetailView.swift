//
//  PostDetailView.swift
//  FITstagram
//
//  Created by Lukáš Cmíral on 14.11.2022.
//

import SwiftUI

struct PostDetailView: View {
    let post: Post
    @State var isHidden = false
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                Group {
                    HStack {
                        Text("\(post.author.username)")
                            .font(.callout)
                            .fontWeight(.semibold)
                            .opacity(isHidden ? 0 : 1)
                        
                        Spacer()
                        
                        Button(action: { isHidden.toggle() }) {
                            Image(systemName: isHidden ? "eye.slash.fill" : "eye.fill")
                                .padding(.horizontal, 8)
                        }
                        Button(action: { }) {
                            Image(systemName: "ellipsis")
                        }
                        .opacity(isHidden ? 0 : 1)
                    }
                }
                .padding(.horizontal, 8)
                
                TabView {
                    ForEach(post.photos, id: \.self) { i in
                        Rectangle()
                            .fill(.clear)
                            .aspectRatio(1, contentMode: .fill)
                            .overlay(
                                RemoteImage(url: URL(string: i)!)
                            )
                            .clipped()
                    }
                }
                .tabViewStyle(.page(indexDisplayMode: .automatic))
                .frame(height: 400)
//                .indexViewStyle(.page(backgroundDisplayMode: .always))
                
                HStack(spacing: 16) {
                    Button(action: { }) {
                        systemImage("heart")
                    }
                    Button(action: { }) {
                        systemImage("message")
                    }
                    Button(action: { }) {
                        systemImage("paperplane")
                    }
                    Spacer()
                    Button(action: { }) {
                        systemImage("bookmark")
                    }
                }
                .padding(.horizontal, 8)
                .opacity(isHidden ? 0 : 1)
                
                Text("\(post.likes) To se mi líbí")
                    .fontWeight(.semibold)
                    .padding(.horizontal, 8)
                    .opacity(isHidden ? 0 : 1)
                
                Group {
                    Text(post.author.username)
                        .fontWeight(.semibold)
                    +
                    Text(" " + post.description)
                }
                .padding(.horizontal, 8)
                .opacity(isHidden ? 0 : 1)
                
                Rectangle()
                    .fill(Color(red: 200/255, green: 200/255, blue: 200/255))
                    .frame(height: 0.5)
                    .padding(.horizontal, 8)
                    .opacity(isHidden ? 0 : 1)
                    
                Spacer()
                
                CommentsView(viewModel: .init(postID: post.id))
                    .padding(.horizontal, 8)
                    .opacity(isHidden ? 0 : 1)
                
                Spacer()
            }
        }
        .background(isHidden ? Color.black : Color.clear)
    }
}

private func systemImage(_ systemName: String) -> some View {
    Image(systemName: systemName)
        .resizable()
        .aspectRatio(contentMode: .fit)
        .frame(height: 22)
}

struct PostDetailView_Previews: PreviewProvider {
    static var previews: some View {
        PostDetailView(
            post: Post(
                id: "1",
                likes: 1024,
                photos: ["https://placeimg.com/640/480/arch", "https://placeimg.com/640/480/tech", "https://placeimg.com/640/480/nature", "https://placeimg.com/640/480/people"],
                description: "Top notch!",
                comments: 256,
//                author: Author(id: "1", username: "igor.ross", firstName: "Igor", lastName: "Rosocha", avatar: "nature")
                author: Author(id: "1", username: "igor.ross")
            )
        )
    }
}
