//
//  AddPostView.swift
//  FITstagram
//
//  Created by Lukáš Cmíral on 13.12.2022.
//

import SwiftUI

struct AddPostView: View {
    @StateObject var postViewModel: PostViewModel = PostViewModel()
    @AppStorage("username") var username = "my_username"
    @State var image: Image?
    @State var imageModel: UIImage?
    @State var description: String = ""
    @State var isImagePickerPresented = false
    @State var imagePicked = false
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        NavigationStack {
            VStack(alignment: .leading) {
                Rectangle()
                    .fill(Color(hue: 0.0, saturation: 0.0, brightness: 0.85))
                    .aspectRatio(1, contentMode: .fit)
                    .overlay {
                        if let image { //měl jsem tam if let postViewModel.image, to ovšem nefungovalo a nepodařilo se mi zatím chybu opravit
                            image
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                        }
                    }
                    .padding(.top, 5.0)
                    .clipped()
                    .overlay(
                        Button {
                            isImagePickerPresented = true
                        } label: {
                            Circle()
                                .fill(.white)
                                .frame(width: 64, height: 64)
                                .overlay(
                                    Image(systemName: (image == nil) ? "plus" : "pencil")
                                        .resizable()
                                        .padding()
                                )
                        }
                    )
                Text("Logged in as \(username)")
                    .padding()
                TextField("Add post description", text: $description)
                    .padding([.leading, .bottom, .trailing])
                Spacer()
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    if postViewModel.isLoading {
                        ProgressView()
                            .progressViewStyle(.circular)
                    } else {
                        Button {
                            print("publish")
                            Task {
                                await postViewModel.addPost(imageModel, description)
                                if postViewModel.errorMsg.isEmpty {
                                    dismiss()
                                }
                            }
                        } label: {
                            Text("Publish").bold()
                        }
                        .alert(postViewModel.errorMsg, isPresented: $postViewModel.showingAlert) {}
                    }
                }
            }
            .fullScreenCover(isPresented: $isImagePickerPresented) {
                ImagePicker(
                    image: $image,
                    isPresented: $isImagePickerPresented,
                    imageModel: $imageModel
                )
            }
        }
    }
}

struct AddPostView_Previews: PreviewProvider {
    static var previews: some View {
        AddPostView()
    }
}
