import SwiftUI

struct AddPostRequest: Encodable {
    let text: String
    let photos: [String]
}

extension UIImage {
    var base64: String? {
        self.jpegData(compressionQuality: 0.2)?.base64EncodedString()
    }
    
    func resizeImageTo(size: CGSize) -> UIImage? { //staženo z webu
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        self.draw(in: CGRect(origin: CGPoint.zero, size: size))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return resizedImage
    }
    
//    func imageWith(newSize: CGSize) -> UIImage { //jiná? nepoužitá možnost
//        let image = UIGraphicsImageRenderer(size: newSize).image { _ in
//            draw(in: CGRect(origin: .zero, size: newSize))
//        }
//        return image.withRenderingMode(renderingMode)
//    }
}

final class PostViewModel: ObservableObject {
    @Published var isLoading = false
    @Published var errorMsg = ""
    @Published var showingAlert = false
    @AppStorage("username") var username = "my_username"
    
    @MainActor
    func addPost(_ image: UIImage?, _ description: String) async {
        isLoading = true
        defer { isLoading = false }

        guard let image else {
            errorMsg = "No image picked!"
            showingAlert = true
            return
        }
        guard !description.isEmpty else {
            errorMsg = "Description not filled!"
            showingAlert = true
            return
        }

        let url = URL(string: "https://fitstagram.ackee.cz/api/feed")!

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = [
            "Authorization": username
        ]

        var base64Image: String = ""
        do {
            let dimensions = try checkSize(image)
            let smallerImage = image.resizeImageTo(size: dimensions)
            base64Image = try convertToBase64(smallerImage)
        } catch {
            errorMsg = error.localizedDescription
            showingAlert = true
            print("[ERROR]=converting image", error.localizedDescription)
        }
        var base64Images: [String] = []
        base64Images.append(base64Image)
        
        let body = AddPostRequest(text: description, photos: base64Images)
        do {
            request.httpBody = try JSONEncoder().encode(body)
        } catch {
            errorMsg = error.localizedDescription
            showingAlert = true
            print("[ERROR]=encoding body", error.localizedDescription)
        }

        do {
            let (_, response) = try await URLSession.shared.data(for: request)
            print(response)
        } catch {
            errorMsg = error.localizedDescription
            showingAlert = true
            print("[ERROR]", error.localizedDescription)
        }
    }
    
    func checkSize(_ imageOpt: UIImage?) throws -> CGSize {
        let image = imageOpt!
        let height = image.size.height
        let width = image.size.width
        let scale = 3.0 //image.scale mi ukazuje 1.0?
        let maxPixels = 2048.0
        let treshhold = maxPixels/scale

        if height > treshhold && width > treshhold { //jen hloupě měním větší rozměry, fotka nebude v měřítku
            return CGSize(width: treshhold, height: treshhold)
        } else if height > treshhold {
            return CGSize(width: width, height: treshhold)
        } else if width > treshhold {
            return CGSize(width: treshhold, height: height)
        } else {
            return CGSize(width: width, height: height)
        }
    }
    
    func convertToBase64(_ image: UIImage?) throws -> String {
        image!.base64!
    }
}
