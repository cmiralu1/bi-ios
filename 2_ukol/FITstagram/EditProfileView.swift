import SwiftUI

struct EditProfileView: View {
    @AppStorage("username") var username = ""
    @State var image: Image?
    @State var imageModel: UIImage?
    @State var isImagePickerPresented = false

    var body: some View {
        VStack(spacing: 16) {
            Rectangle()
                .fill(.gray)
                .aspectRatio(1, contentMode: .fit)
                .overlay {
                    if let image {
                        image
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                    }
                }
                .clipped()
                .overlay(
                    Button {
                        isImagePickerPresented = true
                    } label: {
                        Circle()
                            .fill(.white)
                            .frame(width: 64, height: 64)
                            .overlay(
                                Image(systemName: "pencil")
                                    .resizable()
                                    .padding()
                            )
                    }
                )

            TextField("Username", text: $username)
                .padding()
        }
        .fullScreenCover(isPresented: $isImagePickerPresented) {
            ImagePicker(
                image: $image,
                isPresented: $isImagePickerPresented,
                imageModel: $imageModel
            )
        }
    }
}

struct EditProfileView_Previews: PreviewProvider {
    static var previews: some View {
        EditProfileView()
    }
}

struct ImagePicker: UIViewControllerRepresentable {
    @Binding var image: Image?
    @Binding var isPresented: Bool
    @Binding var imageModel: UIImage?

    func makeUIViewController(context: Context) -> UIImagePickerController {
        let controller = UIImagePickerController()
        controller.delegate = context.coordinator
        return controller
    }

    func updateUIViewController(
        _ uiViewController: UIImagePickerController,
        context: Context
    ) { }

    func makeCoordinator() -> Coordinator {
        Coordinator(image: $image, isPresented: $isPresented, imageModel: $imageModel)
    }

    final class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        @Binding var image: Image?
        @Binding var isPresented: Bool
        @Binding var imageModel: UIImage?

        init(image: Binding<Image?>, isPresented: Binding<Bool>, imageModel: Binding<UIImage?>) {
            self._image = image
            self._isPresented = isPresented
            self._imageModel = imageModel
        }

        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            isPresented = false
        }

        func imagePickerController(
            _ picker: UIImagePickerController,
            didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]
        ) {
            guard let image = info[.originalImage] as? UIImage else { return }
            self.imageModel = image
            self.image = Image(uiImage: image)
            self.isPresented = false
        }
    }


}
