import Foundation

public enum LineStyle {
    case curved
    case straight
}
