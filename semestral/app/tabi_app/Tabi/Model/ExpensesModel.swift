//
//  ExpensesModel.swift
//  Tabi
//
//  Created by Lukáš Cmíral on 08.02.2023.
//

import Foundation

struct Expense: Identifiable, Hashable {
    var id: Int
    var cost: Int
    var type: String
    var description: String?
}

extension Expense {
//    var id: String {ID}
    
    func apiToLocalTypeConvert() -> String {
        switch type {
        case "fuel":
            return "Palivo"
        case "tires":
            return "Pneuservis"
        case "service":
            return "Servis"
        default:
            return "Ostatní"
        }
    }
}

func localToApiTypeConvert(localType: String) -> String {
    switch localType {
    case "Palivo":
        return "fuel"
    case "Pneuservis":
        return "tires"
    case "Servis":
        return "service"
    default:
        return "other"
    }
}

extension Expense: Codable {
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case cost = "czk" //czk - API, cost - local
        case type
        case description
    }

// FYI
//    init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//
//        id = try container.decode(String.self, forKey: .id)
//        let likes = try container.decode([String].self, forKey: .likes)
//        self.likes = likes.count
//        photos = try container.decode([URL].self, forKey: .photos)
//        description = try container.decode(String.self, forKey: .description)
//        comments = try container.decode(Int.self, forKey: .comments)
//        author = try container.decode(Author.self, forKey: .author)
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.container(keyedBy: CodingKeys.self)
//
//        try container.encode(id, forKey: .id)
//        try container.encode(likes, forKey: .likes)
//        try container.encode(photos, forKey: .photos)
//        try container.encode(description, forKey: .description)
//        try container.encode(comments, forKey: .comments)
//        try container.encode(author, forKey: .author)
//    }
}
