//
//  DriversModel.swift
//  Tabi
//
//  Created by Lukáš Cmíral on 08.02.2023.
//

import Foundation

struct Driver: Hashable {
    var username: String
    var firstName: String
    var surName: String
}

extension Driver: Identifiable, Codable {
    var id: String {username}
}
