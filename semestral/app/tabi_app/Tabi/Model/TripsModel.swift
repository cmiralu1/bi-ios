//
//  TripsModel.swift
//  Tabi
//
//  Created by Lukáš Cmíral on 08.02.2023.
//

import Foundation

struct Trip: Hashable {
    var startDateTime_ID: String
    var startDateTime: String
    var destDateTime: String
    var startAddr: String
    var destAddr: String
    var km: Double
    var userUsername: String?
    
//    let formatter = DateFormatter()
//    formatter.dateFormat = "dd-MM-yyyy@HH:mm:ss"
}

extension Trip: Identifiable, Codable {
    var id: String {startDateTime_ID}
    
//    var startDate: Date? {formatter.date(from: startDateTime)}
}
