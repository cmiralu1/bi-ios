//
//  DriversVM.swift
//  Tabi
//
//  Created by Lukáš Cmíral on 08.02.2023.
//

import Foundation

final class DriversVM: ObservableObject {
    @Published private(set) var drivers: [Driver] = []
    @Published var isLoading = false
    @Published var errorOccr = false
    var errorMsg = ""
    
    @MainActor
    func fetchDrivers(triggerAlert: Bool = true) async {
        var request = URLRequest(url: URL(string: serverUrl + "/users")!)
        request.httpMethod = "GET"
        request.timeoutInterval = 5
        
        var statusCode = 400
        do {
            let (data, response) = try await URLSession.shared.data(for: request)
            self.drivers = try JSONDecoder().decode([Driver].self, from: data)
            statusCode = (response as? HTTPURLResponse)?.statusCode ?? 400
            print(statusCode)
        } catch {
            if error.localizedDescription == "The request timed out." {
                statusCode = 504
            } else {
                statusCode = 502
            }
        }
        if (300...599).contains(statusCode) {
            errorMsg = statusCodeDecoder(statusCode)
            if triggerAlert {
                errorOccr = true
            }
            return
        }
    }
    
    @MainActor
    func add(_ driver: Driver) async {
        isLoading = true
        defer { isLoading = false }
        
        var request = URLRequest(url: URL(string: serverUrl + "/users")!)
        request.httpMethod = "POST"
        request.timeoutInterval = 5
        request.allHTTPHeaderFields = [
            "Content-Type": "application/json"
        ]
        
        let body = driver
        do {
            request.httpBody = try JSONEncoder().encode(body)
        } catch {
            errorMsg = error.localizedDescription
            errorOccr = true
            return
        }
        
        var statusCode = 400
        do {
            let (_, response) = try await URLSession.shared.data(for: request)
            statusCode = (response as? HTTPURLResponse)?.statusCode ?? 400
            print(statusCode)
        } catch {
            if error.localizedDescription == "The request timed out." {
                statusCode = 504
            } else {
                statusCode = 502
            }
        }
        if (300...599).contains(statusCode) {
            errorMsg = statusCodeDecoder(statusCode)
            errorOccr = true
            return
        }
        
        if !errorOccr {
            await fetchDrivers()
        }
    }
    
    @MainActor
    func update(_ driver: Driver) async {
        isLoading = true
        defer { isLoading = false }
        
        var request = URLRequest(url: URL(string: serverUrl + "/users/\(driver.username)")!)
        request.httpMethod = "PUT"
        request.timeoutInterval = 5
        request.allHTTPHeaderFields = [
            "Content-Type": "application/json"
        ]
        
        let body = driver
        do {
            request.httpBody = try JSONEncoder().encode(body)
        } catch {
            errorMsg = error.localizedDescription
            errorOccr = true
            return
        }
        
        var statusCode = 400
        do {
            let (_, response) = try await URLSession.shared.data(for: request)
            statusCode = (response as? HTTPURLResponse)?.statusCode ?? 400
            print(statusCode)
        } catch {
            if error.localizedDescription == "The request timed out." {
                statusCode = 504
            } else {
                statusCode = 502
            }
        }
        if (300...599).contains(statusCode) {
            errorMsg = statusCodeDecoder(statusCode)
            errorOccr = true
            return
        }
        
        if !errorOccr {
            await fetchDrivers()
        }
    }
    
    @MainActor
    func remove(_ username: String) async {
        isLoading = true
        defer { isLoading = false }
        
        var request = URLRequest(url: URL(string: serverUrl + "/users/\(username)")!)
        request.httpMethod = "DELETE"
        request.timeoutInterval = 5
        
        var statusCode = 400
        do {
            let (_, response) = try await URLSession.shared.data(for: request)
            statusCode = (response as? HTTPURLResponse)?.statusCode ?? 400
            print(statusCode)
        } catch {
            if error.localizedDescription == "The request timed out." {
                statusCode = 504
            } else {
                statusCode = 502
            }
        }
        if (300...599).contains(statusCode) {
            errorMsg = statusCodeDecoder(statusCode)
            errorOccr = true
            return
        }
        
        if !errorOccr {
            await fetchDrivers()
        }
    }
}
