//
//  TripsVM.swift
//  Tabi
//
//  Created by Lukáš Cmíral on 08.02.2023.
//

import Foundation
import SwiftCSV //https://github.com/swiftcsv/SwiftCSV

struct Status: Identifiable {
    let rowNumber: Int?
    let statusCode: Int
    let description: String
    let id = UUID()
}

final class TripsVM: ObservableObject {
    @Published private(set) var trips: [Trip] = []
    @Published var results: [Status] = []
    @Published var isLoading = false
    @Published var errorOccr = false
    var errorMsg = ""
    
    func updateUserLocally(_ index: Int, _ username: String) {
        trips[index].userUsername = username
    }
    
    @MainActor
    func fetchTrips(triggerAlert: Bool = true) async {
        var request = URLRequest(url: URL(string: serverUrl + "/trips")!)
        request.httpMethod = "GET"
        request.timeoutInterval = 5
        
        var statusCode = 400
        do {
            let (data, response) = try await URLSession.shared.data(for: request)
            self.trips = try JSONDecoder().decode([Trip].self, from: data)
            statusCode = (response as? HTTPURLResponse)?.statusCode ?? 400
            print(statusCode)
        } catch {
            if error.localizedDescription == "The request timed out." {
                statusCode = 504
            } else {
                statusCode = 502
            }
        }
        if (300...599).contains(statusCode) {
            errorMsg = statusCodeDecoder(statusCode)
            if triggerAlert {
                errorOccr = true
            }
            return
        }
    }
    
    @MainActor
    func addAll(_ url: URL, autoAssignUser: Bool = false) async {
        isLoading = true
        defer { isLoading = false }

        guard url.startAccessingSecurityScopedResource() else {
            results.append(Status(rowNumber: nil, statusCode: 1, description: "Soubor nelze otevřít."))
            return
        }
        
        var csv: CSV<Named>!
        do {
            csv = try CSV<Named>(url: url, delimiter: .comma)
            url.stopAccessingSecurityScopedResource()
        } catch {
            print(error.localizedDescription)
            results.append(Status(rowNumber: nil, statusCode: 2, description: "Soubor nelze otevřít."))
            return
        }
        
        var trip: Trip = Trip(startDateTime_ID: "", startDateTime: "", destDateTime: "", startAddr: "", destAddr: "", km: 0, userUsername: nil)
        var rowNumber = 1
        for row in csv.rows {
            //import csv values
            let strStartDateTime = row["Čas odjezdu"] ?? ""
            let strDestDateTime = row["Čas příjezdu"] ?? ""
            trip.startAddr = row["Začátek cesty"] ?? ""
            trip.destAddr = row["Cíl"] ?? ""
            //import km and convert to double
            let km = row["Ujetá vzdálenost (km)"] ?? "0,0"
            let kmWithDot = km.replacingOccurrences(of: ",", with: ".")
            trip.km = Double(kmWithDot) ?? 0.0
            //import dates and shape them to the accepted format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let startDate = dateFormatter.date(from: strStartDateTime)
            let destDate = dateFormatter.date(from: strDestDateTime)
            dateFormatter.dateFormat = "d. M. yyyy HH:mm:ss"
            trip.startDateTime = dateFormatter.string(from: startDate ?? Date())
            trip.destDateTime = dateFormatter.string(from: destDate ?? Date())
            dateFormatter.dateFormat = "dd-MM-yyyy@HH:mm:ss"
            trip.startDateTime_ID = dateFormatter.string(from: startDate ?? Date())
            
            if autoAssignUser {
                if trip.startAddr.contains("Větrušice") || trip.destAddr.contains("Větrušice") {
                    trip.userUsername = "cmiralu1"
                }
                else if trip.startAddr.contains("Kunešova") || trip.destAddr.contains("Kunešova") {
                    trip.userUsername = "cmiralu1"
                }
                else if trip.startAddr.contains("Děčín") || trip.destAddr.contains("Děčín") {
                    trip.userUsername = "vondrJan"
                }
                else if trip.startAddr.contains("Čistovická") || trip.destAddr.contains("Čistovická") {
                    trip.userUsername = "vondrJan"
                }
                else {
                    trip.userUsername = nil
                }
            }
            
            let statusCode = await add(trip)
            results.append(Status(rowNumber: rowNumber, statusCode: statusCode, description: statusCodeDecoder(statusCode)))
            rowNumber += 1
            print(statusCode)

            if (500...599).contains(statusCode) || statusCode == 3 || statusCode == 4 {
                return
            }
        }
        await fetchTrips()
    }
    
    @MainActor
    func add(_ trip: Trip) async -> Int {
        var request = URLRequest(url: URL(string: serverUrl + "/trips")!)
        request.httpMethod = "POST"
        request.timeoutInterval = 5
        request.allHTTPHeaderFields = [
            "Content-Type": "application/json"
        ]
        
        var statusCode = 3
        let body = trip
        do {
            request.httpBody = try JSONEncoder().encode(body)
        } catch {
            return 4
        }
        
        do {
            let (_, response) = try await URLSession.shared.data(for: request)
            statusCode = (response as? HTTPURLResponse)?.statusCode ?? 400
            if statusCode != 200 {
                print(trip.startDateTime_ID, " ", statusCode)
            }
        } catch {
            if error.localizedDescription == "The request timed out." {
                statusCode = 504
            } else {
                statusCode = 502
            }
        }
        return statusCode
    }
    
    @MainActor
    func update(_ trip: Trip) async {
        isLoading = true
        defer { isLoading = false }
        
        var request = URLRequest(url: URL(string: serverUrl + "/trips/\(trip.startDateTime_ID)")!)
        request.httpMethod = "PUT"
        request.timeoutInterval = 5
        request.allHTTPHeaderFields = [
            "Content-Type": "application/json"
        ]
        
        let body = trip
        do {
            request.httpBody = try JSONEncoder().encode(body)
        } catch {
            errorMsg = error.localizedDescription
            errorOccr = true
            return
        }
        
        var statusCode = 400
        do {
            let (_, response) = try await URLSession.shared.data(for: request)
            statusCode = (response as? HTTPURLResponse)?.statusCode ?? 400
            print(statusCode)
        } catch {
            if error.localizedDescription == "The request timed out." {
                statusCode = 504
            } else {
                statusCode = 502
            }
        }
        if (300...599).contains(statusCode) {
            errorMsg = statusCodeDecoder(statusCode)
            errorOccr = true
            return
        }
    }
    
    @MainActor
    func removeAll() async {
        isLoading = true
        defer { isLoading = false }
        
        var request = URLRequest(url: URL(string: serverUrl + "/trips")!)
        request.httpMethod = "DELETE"
        request.timeoutInterval = 5
        
        var statusCode = 400
        do {
            let (_, response) = try await URLSession.shared.data(for: request)
            statusCode = (response as? HTTPURLResponse)?.statusCode ?? 400
            print(statusCode)
        } catch {
            if error.localizedDescription == "The request timed out." {
                statusCode = 504
            } else {
                statusCode = 502
            }
        }
        if (300...599).contains(statusCode) {
            errorMsg = statusCodeDecoder(statusCode)
            errorOccr = true
            return
        }
        trips.removeAll()
    }
}

func statusCodeDecoder(_ statusCode: Int) -> String {
    switch statusCode {
    case 1, 2:
        return "Soubor nelze otevřít."
    case 3:
        return "Chyba při ukládání záznamu."
    case 4:
        return "Chyba při enkódování záznamu."
    case 200...299:
        return "Záznam úspěšně importován."
    case 404:
        return "Záznam s tímto ID nenalezen."
    case 409:
        return "Záznam s tímto ID již existuje."
    case 503, 504:
        return "Nelze navázat spojení se serverem."
    default:
        return "Nastala chyba při komunikaci se serverem."
    }
}
