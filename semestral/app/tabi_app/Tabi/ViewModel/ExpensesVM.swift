//
//  ExpensesVM.swift
//  Tabi
//
//  Created by Lukáš Cmíral on 08.02.2023.
//

import Foundation

struct ExpenseSummary {
    var total: Int = 0
    var fuel: Int = 0
    var tires: Int = 0
    var service: Int = 0
    var other: Int = 0
    
    func toArray() -> [Double] {
        let array = [Double(fuel), Double(tires), Double(service), Double(other)]
        return array
    }
}

final class ExpensesVM: ObservableObject {
    @Published private(set) var expenses: [Expense] = []
    @Published var isLoading = false
    @Published var errorOccr = false
    var errorMsg = ""
    
    func calculateExpenses() -> ExpenseSummary {
        var expenseSummary = ExpenseSummary()
        for expense in expenses {
            expenseSummary.total += expense.cost
            switch expense.type {
            case "fuel":
                expenseSummary.fuel += expense.cost
            case "tires":
                expenseSummary.tires += expense.cost
            case "service":
                expenseSummary.service += expense.cost
            default:
                expenseSummary.other += expense.cost
            }
        }
        return expenseSummary
    }

    @MainActor
    func fetchExpenses(triggerAlert: Bool = true) async {
        var request = URLRequest(url: URL(string: serverUrl + "/costs")!)
        request.httpMethod = "GET"
        request.timeoutInterval = 5
        
        var statusCode = 400
        do {
            let (data, response) = try await URLSession.shared.data(for: request)
            self.expenses = try JSONDecoder().decode([Expense].self, from: data)
            statusCode = (response as? HTTPURLResponse)?.statusCode ?? 400
            print(statusCode)
        } catch {
            if error.localizedDescription == "The request timed out." {
                statusCode = 504
            } else {
                statusCode = 502
            }
        }
        if (300...599).contains(statusCode) {
            errorMsg = statusCodeDecoder(statusCode)
            if triggerAlert {
                errorOccr = true
            }
            return
        }
    }
    
    @MainActor
    func add(_ expense: Expense) async {
        isLoading = true
        defer { isLoading = false }
        
        var request = URLRequest(url: URL(string: serverUrl + "/costs")!)
        request.httpMethod = "POST"
        request.timeoutInterval = 5
        request.allHTTPHeaderFields = [
            "Content-Type": "application/json"
        ]
        
        let body = expense
        do {
            request.httpBody = try JSONEncoder().encode(body)
        } catch {
            errorMsg = error.localizedDescription
            errorOccr = true
            return
        }
        
        var statusCode = 400
        do {
            let (_, response) = try await URLSession.shared.data(for: request)
            statusCode = (response as? HTTPURLResponse)?.statusCode ?? 400
            print(statusCode)
        } catch {
            if error.localizedDescription == "The request timed out." {
                statusCode = 504
            } else {
                statusCode = 502
            }
        }
        if (300...599).contains(statusCode) {
            errorMsg = statusCodeDecoder(statusCode)
            errorOccr = true
            return
        }
        
        if !errorOccr {
            await fetchExpenses()
        }
    }
    
    @MainActor
    func update(_ expense: Expense) async {
        isLoading = true
        defer { isLoading = false }
        
        var request = URLRequest(url: URL(string: serverUrl + "/costs/\(expense.id)")!)
        request.httpMethod = "PUT"
        request.timeoutInterval = 5
        request.allHTTPHeaderFields = [
            "Content-Type": "application/json"
        ]
        
        let body = expense
        do {
            request.httpBody = try JSONEncoder().encode(body)
        } catch {
            errorMsg = error.localizedDescription
            errorOccr = true
            return
        }
        
        var statusCode = 400
        do {
            let (_, response) = try await URLSession.shared.data(for: request)
            statusCode = (response as? HTTPURLResponse)?.statusCode ?? 400
            print(statusCode)
        } catch {
            if error.localizedDescription == "The request timed out." {
                statusCode = 504
            } else {
                statusCode = 502
            }
        }
        if (300...599).contains(statusCode) {
            errorMsg = statusCodeDecoder(statusCode)
            errorOccr = true
            return
        }
        
        if !errorOccr {
            await fetchExpenses()
        }
    }
    
    @MainActor
    func remove(_ id: Int) async {
        isLoading = true
        defer { isLoading = false }
        
        var request = URLRequest(url: URL(string: serverUrl + "/costs/\(id)")!)
        request.httpMethod = "DELETE"
        request.timeoutInterval = 5
        
        var statusCode = 400
        do {
            let (_, response) = try await URLSession.shared.data(for: request)
            statusCode = (response as? HTTPURLResponse)?.statusCode ?? 400
            print(statusCode)
        } catch {
            if error.localizedDescription == "The request timed out." {
                statusCode = 504
            } else {
                statusCode = 502
            }
        }
        if (300...599).contains(statusCode) {
            errorMsg = statusCodeDecoder(statusCode)
            errorOccr = true
            return
        }
        
        if !errorOccr {
            await fetchExpenses()
        }
    }
    
    @MainActor
    func removeAll() async {
        isLoading = true
        defer { isLoading = false }
        
        var request = URLRequest(url: URL(string: serverUrl + "/costs")!)
        request.httpMethod = "DELETE"
        request.timeoutInterval = 5
        
        var statusCode = 400
        do {
            let (_, response) = try await URLSession.shared.data(for: request)
            statusCode = (response as? HTTPURLResponse)?.statusCode ?? 400
            print(statusCode)
        } catch {
            if error.localizedDescription == "The request timed out." {
                statusCode = 504
            } else {
                statusCode = 502
            }
        }
        if (300...599).contains(statusCode) {
            errorMsg = statusCodeDecoder(statusCode)
            errorOccr = true
            return
        }
        expenses.removeAll()
    }
    
}

