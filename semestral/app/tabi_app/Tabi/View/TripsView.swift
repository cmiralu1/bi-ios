//
//  TripsView.swift
//  Tabi
//
//  Created by Lukáš Cmíral on 06.02.2023.
//

import SwiftUI

struct TripsView: View {
    @EnvironmentObject var driversVM: DriversVM
    @EnvironmentObject var tripsVM: TripsVM
    @State var sheetDisplayed = false
    @State var filesDisplayed = false
    @State var fileURL: URL!
    @State var errorMgs = ""
    @State var errorOccr = false
    @State var confirmationAlertDisplayed = false
    
    var body: some View {
        NavigationStack {
            ScrollView {
//                LazyVGrid(columns: [GridItem()]) { //bug - user remained in memory, but enables refreshable
//                LazyVStack {} //bug - user remained in memory, but enables refreshable
//                VStack { //crashes with more data
                LazyVStack {
                    ForEach(Array(tripsVM.trips.enumerated()), id: \.offset) { index, trip in
                        TripView(trip: trip, index: index)
                    }
                }
            }
            .refreshable {
                Task {
                    await driversVM.fetchDrivers()
                    await tripsVM.fetchTrips()
                }
            }
            .alert(errorMgs, isPresented: $errorOccr) {}
            .alert(tripsVM.errorMsg, isPresented: $tripsVM.errorOccr) {}
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button {
                        filesDisplayed = true
                    } label: {
                        Image(systemName: "plus")
                    }
                }
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    if tripsVM.isLoading {
                        ProgressView()
                            .progressViewStyle(.circular)
                    }
                }
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    Button {
                        confirmationAlertDisplayed = true
                    } label: {
                        Image(systemName: "trash")
                    }
                    .confirmationDialog("Chcete smazat všechny jízdy?", isPresented: $confirmationAlertDisplayed, titleVisibility: .visible) {
                        Button("Smazat", role: .destructive) {
                            Task {
                                await tripsVM.removeAll()
                            }
                        }
                        Button("Zrušit", role: .cancel) {}
                    }
                    
                }
            }
            .sheet(isPresented: $sheetDisplayed) {
                NavigationView {
                    ImportDataView(url: fileURL)
                }
            }
            .fileImporter(isPresented: $filesDisplayed, allowedContentTypes: [.commaSeparatedText], allowsMultipleSelection: false) { result in
                do {
                    fileURL = try result.get().first
                    sheetDisplayed = true
                } catch {
                    errorMgs = "Soubor nelze otevřít."
                    errorOccr = true
                }
            }
            .navigationTitle("Jízdy")
        }
    }
}

struct TripView: View {
    @EnvironmentObject var driversVM: DriversVM
    @EnvironmentObject var tripsVM: TripsVM
    @Environment(\.colorScheme) var colorScheme
    @State var trip: Trip
    @State var user: String
    @State var index: Int
    
    init(trip: Trip, index: Int) {
        self._trip = State(initialValue: trip)
        self._user = State(initialValue: trip.userUsername ?? "")
        self._index =  State(initialValue: index)
    }
    
    var body: some View {
        NavigationStack {
            VStack(alignment: .leading) {
                MapView(positions: [GeoPoint(address: trip.startAddr),
                                    GeoPoint(address: trip.destAddr)])
                .frame(height: 200)
                .cornerRadius(20, corners: [.topLeft, .topRight])
                HStack(alignment: .center) {
                    VStack(alignment: .leading) {
                        Text("Začátek cesty:")
                            .font(.callout)
                            .foregroundColor(.gray)
                        Text(trip.startDateTime)
                            .font(.callout)
                        Text(trip.startAddr)
                            .font(.callout)
                            .padding(.bottom)
                        
                        Text("Konec cesty:")
                            .font(.callout)
                            .foregroundColor(.gray)
                        Text(trip.destDateTime)
                            .font(.callout)
                        Text(trip.destAddr)
                            .font(.callout)
                            .padding(.bottom)
                    }
                    Spacer()
                    VStack(spacing: -10.0) {
                        Text(String(trip.km))
                            .font(.title)
                            .fontWeight(.black)
                        Text("km")
                            .font(.title)
                            .fontWeight(.black)
                    }
                }
                .padding(.horizontal)
                Picker("Řidič", selection: $user) {
                    ForEach(driversVM.drivers, id: \.username) { driver in
                        Text(driver.firstName)
                    }
                }
                .onChange(of: user) { _ in
                    Task {
                        trip.userUsername = user
                        tripsVM.updateUserLocally(index, user)
                        await tripsVM.update(trip)
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
                .padding([.leading, .bottom, .trailing])
   
            }
            .background(RoundedRectangle(cornerRadius: 20)
                .fill(colorScheme == .light ? .white : Color(red: 0.15, green: 0.15, blue: 0.15))
                .shadow(color: colorScheme == .light ? .black.opacity(0.33) : .white.opacity(0.25), radius: 8.0))
            .padding()
        }
    }
}

struct ImportDataView: View {
    @EnvironmentObject var tripsVM: TripsVM
    @EnvironmentObject var driversVM: DriversVM
    @Environment(\.dismiss) var dismiss
    @Environment (\.horizontalSizeClass) var sizeClass
    @State var alreadyClicked = false
    let url: URL
    
    var body: some View {
        NavigationStack {
            VStack {
                Group {
                    Button {
                        Task {
                            let autoAssignCmiralu1 = driversVM.drivers.contains(where: {$0.username == "cmiralu1"})
                            let autoAssignVondrJan = driversVM.drivers.contains(where: {$0.username == "vondrJan"})
                            let autoAssign = autoAssignCmiralu1 && autoAssignVondrJan
                            await tripsVM.addAll(url, autoAssignUser: autoAssign)
                        }
                        alreadyClicked = true
                    } label: {
                        if tripsVM.isLoading {
                            Text("Načítání    ")
                                .fontWeight(.bold)
                            ProgressView()
                                .progressViewStyle(.circular)
                        } else {
                            Text(alreadyClicked ? "Načítání ukončeno" : "Načíst soubor \(url.lastPathComponent)")
                                .fontWeight(.bold)
                        }
                    }
                    .buttonStyle(.borderedProminent)
                    .disabled(alreadyClicked)
                }
                .padding(.vertical)
                Table(tripsVM.results) {
                    TableColumn("Záznam") { result in
                        HStack {
                            Text("\(result.rowNumber ?? 0)")
                            if sizeClass == .compact {
                                if (200...299).contains(result.statusCode) {
                                    Text(Image(systemName: "checkmark.circle.fill"))
                                        .foregroundColor(.green)
                                } else {
                                    Text(Image(systemName: "multiply.circle.fill"))
                                        .foregroundColor(.red)
                                }
                                Text("\(result.description)")
                                    .font(.callout)
                            }
                        }
                    }
                    TableColumn("Úspěch") { result in
                        if (200...299).contains(result.statusCode) {
                            Text(Image(systemName: "checkmark.circle.fill"))
                                .foregroundColor(.green)
                        } else {
                            Text(Image(systemName: "multiply.circle.fill"))
                                .foregroundColor(.red)
                        }
                    }
                    TableColumn("Status") { result in
                        Text("\(result.statusCode)")
                    }
                    TableColumn("Popis") { result in
                        Text("\(result.description)")
                    }
                }
            }
        }
        .toolbar {
            ToolbarItem(placement: .cancellationAction) {
                Button("Zavřít") {
                    tripsVM.results.removeAll()
                    dismiss()
                }
            }
        }
        .alert(tripsVM.errorMsg, isPresented: $tripsVM.errorOccr) {}
    }
}

extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( RoundedCorner(radius: radius, corners: corners) )
    }
}

struct RoundedCorner: Shape {
    
    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners
    
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}

struct ImportDataView_Previews: PreviewProvider {
    static var previews: some View {
        ImportDataView(url: URL(string: "file://private/var/mobile/Library/Mobile%20Documents/com~apple~CloudDocs/trips1.CSV")!)
            .environmentObject(TripsVM())
    }
}

struct TripsView_Previews: PreviewProvider {
    static var previews: some View {
        return TripsView()
            .environmentObject(DriversVM())
            .environmentObject(TripsVM())
    }
}
