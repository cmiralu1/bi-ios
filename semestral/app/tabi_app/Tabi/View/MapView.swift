//
//  MapView.swift
//  Tabi
//
//  Created by Lukáš Cmíral on 10.02.2023.
//

import SwiftUI
import MapKit

//struct MapView: UIViewRepresentable {
//    var address1: String
//    var address2: String
//
//    func makeUIView(context: Context) -> MKMapView {
//        let mapView = MKMapView()
//        mapView.delegate = context.coordinator
//
//        return mapView
//    }
//
//    func updateUIView(_ view: MKMapView, context: Context) {
//        let geocoder1 = CLGeocoder()
//        let geocoder2 = CLGeocoder()
//
//        let coord1 = geocodeAddress(address: address1, geocoder: geocoder1, mapView: view)
//        let coord2 = geocodeAddress(address: address2, geocoder: geocoder2, mapView: view)
//
//        print(coord1)
//        print(coord1)
//
//
//        let avgLat = (coord1.latitude + coord2.latitude) / 2
//        let avgLon = (coord1.longitude + coord2.longitude) / 2
//
//        print(avgLat)
//        print(avgLon)
//
//        let center = CLLocationCoordinate2DMake(avgLat, avgLon)
//        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
//        let region = MKCoordinateRegion(center: center, span: span)
//        view.setRegion(region, animated: true)
//    }
//
//    func makeCoordinator() -> Coordinator {
//        Coordinator(self)
//    }
//
//    class Coordinator: NSObject, MKMapViewDelegate {
//        var parent: MapView
//
//        init(_ parent: MapView) {
//            self.parent = parent
//        }
//    }
//
//    private func geocodeAddress(address: String, geocoder: CLGeocoder, mapView: MKMapView) -> CLLocationCoordinate2D {
//        var coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D()
//
//        geocoder.geocodeAddressString(address) { (placemarks, error) in
//            if let placemark = placemarks?.first {
//                coordinate = placemark.location!.coordinate
//                let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
//                print("COORDINATE ZDE:", coordinate)
//                let region = MKCoordinateRegion(center: coordinate, span: span)
//                mapView.setRegion(region, animated: true)
//                mapView.addAnnotation(MKPointAnnotation(__coordinate: coordinate, title: address, subtitle: ""))
//            }
//        }
//        print("COORDINATE ZDE XX:", coordinate)
//        return coordinate
//    }
//}

//===================================================================================================================================================

//struct MapView2: UIViewRepresentable {
//    var address1: String
//    var address2: String
//
//    func makeUIView(context: Context) -> MKMapView {
//        let mapView = MKMapView()
//        mapView.delegate = context.coordinator
//
//        return mapView
//    }
//
//    func updateUIView(_ view: MKMapView, context: Context) {
//        let geocoder1 = CLGeocoder()
//        let geocoder2 = CLGeocoder()
//
//        geocodeAddress(address: address1, geocoder: geocoder1, mapView: view, completion: { coordinate1 in
//            let centerLatitude = (view.annotations[0].coordinate.latitude + coordinate1.latitude) / 2
//            let centerLongitude = (view.annotations[0].coordinate.longitude + coordinate1.longitude) / 2
//            let centerCoordinate = CLLocationCoordinate2D(latitude: centerLatitude, longitude: centerLongitude)
//
////            let span = MKCoordinateSpan(latitudeDelta: abs(view.annotations[0].coordinate.latitude - coordinate1.latitude) * 0.5,
////                                        longitudeDelta: abs(view.annotations[0].coordinate.longitude - coordinate1.longitude) * 0.5)
//
//            var maxSpan = max(abs(view.annotations[0].coordinate.latitude - coordinate1.latitude), abs(view.annotations[0].coordinate.longitude - coordinate1.longitude))
//            maxSpan = max(maxSpan, 0.005)
//            let span = MKCoordinateSpan(latitudeDelta: maxSpan * 2, longitudeDelta: maxSpan * 2)
//
//            let region = MKCoordinateRegion(center: centerCoordinate, span: span)
//            view.setRegion(region, animated: true)
//        })
//
//        geocodeAddress(address: address2, geocoder: geocoder2, mapView: view, completion: { coordinate2 in
//            let centerLatitude = (view.annotations[0].coordinate.latitude + coordinate2.latitude) / 2
//            let centerLongitude = (view.annotations[0].coordinate.longitude + coordinate2.longitude) / 2
//            let centerCoordinate = CLLocationCoordinate2D(latitude: centerLatitude, longitude: centerLongitude)
//
////            let span = MKCoordinateSpan(latitudeDelta: abs(view.annotations[0].coordinate.latitude - coordinate2.latitude) * 0.5,
////                                        longitudeDelta: abs(view.annotations[0].coordinate.longitude - coordinate2.longitude) * 0.5)
//
//            var maxSpan = max(abs(view.annotations[0].coordinate.latitude - coordinate2.latitude), abs(view.annotations[0].coordinate.longitude - coordinate2.longitude))
//            maxSpan = max(maxSpan, 0.005)
//            let span = MKCoordinateSpan(latitudeDelta: maxSpan * 2, longitudeDelta: maxSpan * 2)
//
//            let region = MKCoordinateRegion(center: centerCoordinate, span: span)
//            view.setRegion(region, animated: true)
//        }
//        )
//    }
//
//    func makeCoordinator() -> Coordinator {
//        Coordinator(self)
//    }
//
//    class Coordinator: NSObject, MKMapViewDelegate {
//        var parent: MapView2
//
//        init(_ parent: MapView2) {
//            self.parent = parent
//        }
//    }
//
//    private func geocodeAddress(address: String, geocoder: CLGeocoder, mapView: MKMapView, completion: @escaping (CLLocationCoordinate2D) -> ()) {
//        geocoder.geocodeAddressString(address) { (placemarks, error) in
//            if let placemark = placemarks?.first {
//                let coordinate = placemark.location!.coordinate
//                mapView.addAnnotation(MKPointAnnotation(__coordinate: coordinate, title: address, subtitle: ""))
//                completion(coordinate)
//            }
//        }
//    }
//}

//===================================================================================================================================================

func forwardGeocoding(_ address: String) async -> CLPlacemark? {
    let geocoder = CLGeocoder()
    var myPlacemark: CLPlacemark?

    do {
        myPlacemark = try await geocoder.geocodeAddressString(address).first
    } catch {
        print("ERROR geocoding")
    }
    return myPlacemark
}

struct GeoPoint {
    var address: String
    var latitude: CLLocationDegrees {myPlacemark?.location?.coordinate.latitude ?? 50.0842758} //default to Prague City Centre
    var longitude: CLLocationDegrees {myPlacemark?.location?.coordinate.longitude ?? 14.4114747}
    var myPlacemark: CLPlacemark?
}

struct MapView : View {
    @State var positions : [GeoPoint]

    struct IdentifiablePoint: Identifiable {
        var id = UUID()
        var position : GeoPoint
    }

    var centerOfPoints : (center: CLLocationCoordinate2D, span: MKCoordinateSpan) {
        var minLat = 91.0
        var maxLat = -91.0
        var minLon = 181.0
        var maxLon = -181.0

        for i in positions {
            maxLat = max(maxLat, i.latitude)
            minLat = min(minLat, i.latitude)
            maxLon = max(maxLon, i.longitude)
            minLon = min(minLon, i.longitude)
        }

        let center = CLLocationCoordinate2D(latitude: (maxLat + minLat) / 2,
                                           longitude: (maxLon + minLon) / 2)

        let span = MKCoordinateSpan(latitudeDelta: abs(maxLat - minLat) * 1.8,
                                    longitudeDelta: abs(maxLon - minLon) * 1.8)
        return (center: center,
                span: span)
    }

    var body: some View {
        let center = centerOfPoints

        return Map(coordinateRegion: .constant(MKCoordinateRegion(center: center.center, span: center.span)), showsUserLocation: false, annotationItems: positions.map { IdentifiablePoint(position: $0)}) { (point) in
            MapMarker(coordinate: CLLocationCoordinate2D(latitude: point.position.latitude, longitude: point.position.longitude))
//            MapAnnotation(coordinate: CLLocationCoordinate2D(latitude: point.position.latitude,
//                                                             longitude: point.position.longitude)) {
//                Text("lala")
//            }
        }
        .onAppear {
            if positions.count >= 1 {
                Task {
                    positions[0].myPlacemark = await forwardGeocoding(positions[0].address)
                }
                Task {
                    positions[1].myPlacemark = await forwardGeocoding(positions[1].address)
                }
            }
        }
    }
}

//===================================================================================================================================================

//struct MapView2_Previews: PreviewProvider {
//    static var previews: some View {
//        MapView2(address1: "Střížkovská 340, 180 00 Praha, Praha, Česko",
//                 address2: "Šimůnkova 1605/11, 182 00 Praha, Praha, Česko")
//    }
//}
        
struct MapView_Previews: PreviewProvider {
    static var previews: some View {
//        MapView2(address1: "Střížkovská 340, 180 00 Praha, Praha, Česko",
//                address2: "Pohořelec 150/5, 118 00 Praha, Praha, Česko")
        
        
//        MapView(positions: [GeoPoint(latitude: 43.0, longitude: -75),
//                                 GeoPoint(latitude: 41.0, longitude: -75)])
             
        MapView(positions: [GeoPoint(address: "Střížkovská 340, 180 00 Praha, Praha, Česko"),
                                 GeoPoint(address: "Pohořelec 150/5, 118 00 Praha, Praha, Česko")])
        
    }
}

