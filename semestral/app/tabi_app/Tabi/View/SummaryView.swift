//
//  SummaryView.swift
//  Tabi
//
//  Created by Lukáš Cmíral on 05.02.2023.
//

import SwiftUI
import SwiftUICharts //https://github.com/AppPear/ChartView/tree/feat/add-new-readme

struct DriverStats {
    var expense: Int = 0
    var km: Double = 0.0
    var tripCount: Int = 0
}

struct SummaryView: View {
    @EnvironmentObject var driversVM: DriversVM
    @EnvironmentObject var tripsVM: TripsVM
    @EnvironmentObject var expensesVM: ExpensesVM
    @Binding var expenseSummary: ExpenseSummary
    
    var body: some View {
        NavigationStack {
            ScrollView {
                VStack(alignment: .leading) {
                    PieChartView(expenseSummary: $expenseSummary)
                    ForEach(driversVM.drivers) { driver in
                        SummaryUserView(driver: driver, driverStats: calculateDriver(username: driver.username))
                    }
                }
                .padding()
            }
            .navigationTitle("Souhrn")
            .alert(driversVM.errorMsg, isPresented: $driversVM.errorOccr) {}
            .refreshable {
                Task {
                    await driversVM.fetchDrivers()
                    await tripsVM.fetchTrips()
                    await expensesVM.fetchExpenses()
                    expenseSummary = expensesVM.calculateExpenses()
                }
            }
        }
        .onAppear {
            expenseSummary = expensesVM.calculateExpenses()
        }
    }
    
    func calculateDriver(username: String) -> DriverStats {
        var driverExpense = DriverStats()
        var totalKm = 0.0
        var unmannedKm = 0.0
        for trip in tripsVM.trips {
            totalKm += trip.km
            if let tripUsername = trip.userUsername {
                if tripUsername == username {
                    driverExpense.km += trip.km
                    driverExpense.tripCount += 1
                }
            }
            if trip.userUsername == nil {
                unmannedKm += trip.km
            }
        }
        if totalKm != 0.0 && driversVM.drivers.count != 0 {
            let driverAloneExpense = Int((driverExpense.km / totalKm) * Double(expenseSummary.total))
            let collectiveExpense = Int((unmannedKm / totalKm) * Double(expenseSummary.total)) / driversVM.drivers.count
            driverExpense.expense = driverAloneExpense + collectiveExpense
        } else if driversVM.drivers.count != 0 {
            let collectiveExpense = expenseSummary.total / driversVM.drivers.count
            driverExpense.expense = collectiveExpense
        }
        return driverExpense
    }
}


struct SummaryUserView: View {
    var driver: Driver
    var driverStats: DriverStats
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(driver.firstName)
                .font(.title2)
                .fontWeight(.semibold)
                .padding(.top)
            Group {
                HStack {
                    Text("Výsledná cena:")
                        .fontWeight(.semibold)
                        .foregroundColor(.gray)
                    Text("\(driverStats.expense) \(Locale.current.currency?.identifier ?? "Kč")")
                        .fontWeight(.semibold)
                }
                HStack {
                    Text("Ujetá vzdálenost:")
                        .fontWeight(.semibold)
                        .foregroundColor(.gray)
                    Text("\(Int(driverStats.km)) km")
                        .fontWeight(.semibold)
                        .foregroundColor(.gray)
                }
                HStack {
                    Text("Počet jízd:")
                        .fontWeight(.semibold)
                        .foregroundColor(.gray)
                    Text("\(driverStats.tripCount)")
                        .fontWeight(.semibold)
                        .foregroundColor(.gray)
                }
            }
            Spacer()    
        }
    }
}

struct PieChartView: View {
    @Environment(\.colorScheme) var colorScheme
    @Binding var expenseSummary: ExpenseSummary
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Celkové výdaje")
                .font(.title2)
                .fontWeight(.semibold)
            Text("Dle kategorie")
                .fontWeight(.semibold)
                .foregroundColor(.gray)
                .padding(.bottom)
            HStack(alignment: .center) {
                PieChart()
                    .data(expenseSummary.toArray())
                    .chartStyle(ChartStyle(backgroundColor: .clear,
                                           foregroundColor: [ColorGradient(Color("MyRed"), Color("MyPurple")),
                                                             ColorGradient(Color("MyPurple"), Color("MyCyan")),
                                                             ColorGradient(Color("MyCyan"), Color("MyGreen")),
                                                             ColorGradient(Color("MyGreen"), Color("MyYellow")),]))
                    .disabled(true) //chart is buggy on touch
                    .shadow(color: colorScheme == .light ? .black.opacity(0.33) : .white.opacity(0.25), radius: 5.0)
                VStack(alignment: .leading, spacing: 8.0) {
                    HStack(alignment: .top) {
                        Circle()
                            .fill(ColorGradient(Color("MyRed"), Color("MyPurple")).linearGradient(from: .bottom, to: .top))
                            .frame(width: 24.0, height: 24.0)
                        VStack(alignment: .leading) {
                            Text("Palivo")
                                .fontWeight(.semibold)
                                .foregroundColor(.gray)
                            Text("\(expenseSummary.fuel) \(Locale.current.currency?.identifier ?? "Kč")")
                                .fontWeight(.semibold)
                        }
                    }
                    HStack(alignment: .top) {
                        Circle()
                            .fill(ColorGradient(Color("MyPurple"), Color("MyCyan")).linearGradient(from: .bottom, to: .top))
                            .frame(width: 24.0, height: 24.0)
                        VStack(alignment: .leading) {
                            Text("Pneuservis")
                                .fontWeight(.semibold)
                                .foregroundColor(.gray)
                            Text("\(expenseSummary.tires) \(Locale.current.currency?.identifier ?? "Kč")")
                                .fontWeight(.semibold)
                        }
                    }
                    HStack(alignment: .top) {
                        Circle()
                            .fill(ColorGradient(Color("MyCyan"), Color("MyGreen")).linearGradient(from: .bottom, to: .top))
                            .frame(width: 24.0, height: 24.0)
                        VStack(alignment: .leading) {
                            Text("Servis")
                                .fontWeight(.semibold)
                                .foregroundColor(.gray)
                            Text("\(expenseSummary.service) \(Locale.current.currency?.identifier ?? "Kč")")
                                .fontWeight(.semibold)
                        }
                    }
                    HStack(alignment: .top) {
                        Circle()
                            .fill(ColorGradient(Color("MyGreen"), Color("MyYellow")).linearGradient(from: .bottom, to: .top))
                            .frame(width: 24.0, height: 24.0)
                        VStack(alignment: .leading) {
                            Text("Ostatní")
                                .fontWeight(.semibold)
                                .foregroundColor(.gray)
                            Text("\(expenseSummary.other) \(Locale.current.currency?.identifier ?? "Kč")")
                                .fontWeight(.semibold)
                        }
                    }
                }
            }
        }
        .padding()
        .background(RoundedRectangle(cornerRadius: 20)
            .fill(colorScheme == .light ? .white : Color(red: 0.15, green: 0.15, blue: 0.15))
            .shadow(color: colorScheme == .light ? .black.opacity(0.33) : .white.opacity(0.25), radius: 8.0))
    }
}

struct SummaryView_Previews: PreviewProvider {
    static var previews: some View {
        let expenseSummary: ExpenseSummary = ExpenseSummary()
        SummaryView(expenseSummary: .constant(expenseSummary))
            .environmentObject(DriversVM())
            .environmentObject(TripsVM())
            .environmentObject(ExpensesVM())
    }
}
