//
//  ExpensesView.swift
//  Tabi
//
//  Created by Lukáš Cmíral on 06.02.2023.
//

import SwiftUI

struct ExpensesView: View {
    @EnvironmentObject var expensesVM: ExpensesVM
    @State var sheetDisplayed = false
    @State var confirmationAlertDisplayed = false
    
    var body: some View {
        NavigationStack {
            List(expensesVM.expenses) { expense in
                NavigationLink {
                    EditExpenseView(expense: expense)
                } label: {
                    HStack {
                        VStack(alignment: .leading) {
                            Text(expense.apiToLocalTypeConvert())
                            if let description = expense.description {
                                Text(description)
                                    .foregroundColor(.gray)
                            }
                        }
                        Spacer()
                        Text("\(expense.cost) \(Locale.current.currency?.identifier ?? "Kč")")
                            .fontWeight(.bold)
                    }
                }
                .swipeActions {
                    Button("Smazat", role: .destructive) {
                        Task {
                            await expensesVM.remove(expense.id)
                        }
                    }
                }
            }
            .sheet(isPresented: $sheetDisplayed) {
                NavigationView {
                    AddExpenseView()
                }
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button {
                        sheetDisplayed = true
                    } label: {
                        Image(systemName: "plus")
                    }
                }
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    if expensesVM.isLoading {
                        ProgressView()
                            .progressViewStyle(.circular)
                    }
                }
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    Button {
                        confirmationAlertDisplayed = true
                    } label: {
                        Image(systemName: "trash")
                    }
                    .confirmationDialog("Chcete smazat všechny výdaje?", isPresented: $confirmationAlertDisplayed, titleVisibility: .visible) {
                        Button("Smazat", role: .destructive) {
                            Task {
                                await expensesVM.removeAll()
                            }
                        }
                        Button("Zrušit", role: .cancel) {}
                    }
                    
                }
            }
            .navigationTitle("Výdaje")
            .refreshable {
                Task {
                    await expensesVM.fetchExpenses()
                }
            }
            .alert(expensesVM.errorMsg, isPresented: $expensesVM.errorOccr) {}
        }
    }
}

struct AddExpenseView: View {
    @EnvironmentObject var expensesVM: ExpensesVM
    @Environment(\.dismiss) var dismiss
    @State var cost: Int?
    @State var description: String = ""
    @State var type: String = "Palivo"
    let types = ["Palivo", "Pneuservis", "Servis", "Ostatní"]
    
    var body: some View {
        VStack {
            Form {
                Picker("Kategorie", selection: $type) {
                    ForEach(types, id: \.self) { type in
                        Text(type)
                    }
                }
                TextField("Cena", value: $cost, format: .currency(code: Locale.current.currency?.identifier ?? "Kč"))
                    .keyboardType(.numberPad)
                TextField("Popis", text: $description)
                    .keyboardType(.default)
            }
        }
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                if expensesVM.isLoading {
                    ProgressView()
                        .progressViewStyle(.circular)
                        .onDisappear {
                            if !expensesVM.errorOccr {
                                dismiss()
                            }
                        }
                } else {
                    Button {
                        Task {
                            await expensesVM.add(Expense(id: 0, cost: cost!, type: localToApiTypeConvert(localType: type), description: description.isEmpty ? nil : description))
                            //id 0 => vytvoří se vlastní ID dle uvážení serveru
                        }
                    } label: {
                        Text("Uložit")
                            .fontWeight(.bold)
                    }
                    .disabled(cost == nil)
                }
            }
        }
        .toolbar {
            ToolbarItem(placement: .cancellationAction) {
                Button("Zavřít") {
                    dismiss()
                }
            }
        }
        .alert(expensesVM.errorMsg, isPresented: $expensesVM.errorOccr) {}
        .navigationTitle("Přidat výdaj")
    }
}

struct EditExpenseView: View {
    @EnvironmentObject var expensesVM: ExpensesVM
    @Environment(\.dismiss) var dismiss
    @State var confirmationAlertDisplayed = false
    @State var expense: Expense
    @State var cost: Int?
    @State var description: String
    @State var type: String
    let types = ["Palivo", "Pneuservis", "Servis", "Ostatní"]
    
    init(expense: Expense) {
        self._expense = State(initialValue: expense)
        self._cost = State(initialValue: expense.cost)
        self._description = State(initialValue: expense.description ?? "")
        self._type = State(initialValue: expense.apiToLocalTypeConvert())
    }
    
    var body: some View {
        EmptyView()
        NavigationStack {
            VStack {
                Form {
                    Section {
                        Picker("Kategorie", selection: $type) {
                            ForEach(types, id: \.self) { type in
                                Text(type)
                            }
                        }
                        TextField("Cena", value: $cost, format: .currency(code: Locale.current.currency?.identifier ?? "Kč"))
                            .keyboardType(.numberPad)
                        TextField("Popis", text: $description)
                            .keyboardType(.default)
                    }
                    Section {
                        Button("Smazat výdaj", role: .destructive) {
                            confirmationAlertDisplayed = true
                        }
                        .confirmationDialog("Chcete smazat výdaj?", isPresented: $confirmationAlertDisplayed, titleVisibility: .visible) {
                            if expensesVM.isLoading {
                                ProgressView()
                                    .progressViewStyle(.circular)
                                    .onDisappear {
                                        if !expensesVM.errorOccr {
                                            dismiss()
                                        }
                                    }
                            } else {
                                Button("Smazat", role: .destructive) {
                                    Task {
                                        await expensesVM.remove(expense.id)
                                    }
                                }
                            }
                            Button("Zrušit", role: .cancel) {}
                        }
                    }
                }
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    if expensesVM.isLoading {
                        ProgressView()
                            .progressViewStyle(.circular)
                            .onDisappear {
                                if !expensesVM.errorOccr {
                                    dismiss()
                                }
                            }
                    } else {
                        Button {
                            Task {
                                await expensesVM.update(Expense(id: expense.id, cost: cost!, type: localToApiTypeConvert(localType: type), description: description.isEmpty ? nil : description))
                            }
                        } label: {
                            Text("Uložit")
                                .fontWeight(.bold)
                        }
                        .disabled(cost == nil)
                    }
                }
            }
            .navigationTitle(type)
        }
    }
}

struct ExpensesView_Previews: PreviewProvider {
    static var previews: some View {
        ExpensesView()
            .environmentObject(ExpensesVM())
    }
}

struct AddExpenseView_Previews: PreviewProvider {
    static var previews: some View {
        AddExpenseView()
            .environmentObject(ExpensesVM())
    }
}

struct EditExpenseView_Previews: PreviewProvider {
    static var previews: some View {
        EditExpenseView(expense: Expense(id: 1, cost: 399, type: "Palivo", description: "Shell"))
            .environmentObject(ExpensesVM())
    }
}
