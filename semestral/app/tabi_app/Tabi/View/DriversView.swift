//
//  DriversView.swift
//  Tabi
//
//  Created by Lukáš Cmíral on 06.02.2023.
//

import SwiftUI

struct DriversView: View {
    @EnvironmentObject var driversVM: DriversVM
    @State var sheetDisplayed = false
    
    var body: some View {
        NavigationStack {
            List(driversVM.drivers) { driver in
                NavigationLink {
                    EditDriverView(driver: driver)
                } label: {
                    Text(driver.firstName)
                }
                .swipeActions {
                    Button("Smazat", role: .destructive) {
                        Task {
                            await driversVM.remove(driver.username)
                        }
                    }
                }
            }
            .sheet(isPresented: $sheetDisplayed) {
                NavigationView {
                    AddDriverView()
                }
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button {
                        sheetDisplayed = true
                    } label: {
                        Image(systemName: "plus")
                    }
                }
            }
            .navigationTitle("Řidiči")
            .refreshable {
                Task {
                    await driversVM.fetchDrivers()
                }
            }
            .alert(driversVM.errorMsg, isPresented: $driversVM.errorOccr) {}
        }
    }
}

struct AddDriverView: View {
    @EnvironmentObject var driversVM: DriversVM
    @Environment(\.dismiss) var dismiss
    @State var driver: Driver = Driver(username: "", firstName: "", surName: "")
    let categories = ["Palivo", "Pneuservis", "Servis", "Ostatní"]
    
    var body: some View {
        VStack {
            Form {
                TextField("Uživatelské jméno", text: $driver.username)
                TextField("Křestní jméno", text: $driver.firstName)
                TextField("Příjmení", text: $driver.surName)
            }
        }
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                if driversVM.isLoading {
                    ProgressView()
                        .progressViewStyle(.circular)
                        .onDisappear {
                            if !driversVM.errorOccr {
                                dismiss()
                            }
                        }
                } else {
                    Button {
                        Task {
                            await driversVM.add(driver)
                        }
                    } label: {
                        Text("Uložit")
                            .fontWeight(.bold)
                    }
                    .disabled(driver.username.isEmpty)
                    .disabled(driver.firstName.isEmpty)
                    .disabled(driver.surName.isEmpty)
                }
            }
        }
        .toolbar {
            ToolbarItem(placement: .cancellationAction) {
                Button("Zavřít") {
                    dismiss()
                }
            }
        }
        .alert(driversVM.errorMsg, isPresented: $driversVM.errorOccr) {}
        .navigationTitle("Přidat řidiče")
    }
}

struct EditDriverView: View {    
    @EnvironmentObject var driversVM: DriversVM
    @Environment(\.dismiss) var dismiss
    @State var driver: Driver
    let categories = ["Palivo", "Pneuservis", "Servis", "Ostatní"]
    @State var confirmationAlertDisplayed = false
    
    var body: some View {
        NavigationStack {
            VStack {
                Form {
                    Section {
                        Text(driver.username)
                            .foregroundColor(.gray)
                        TextField("Křestní jméno", text: $driver.firstName)
                        TextField("Příjmení", text: $driver.surName)
                    }
                    Section {
                        Button("Smazat řidiče", role: .destructive) {
                            confirmationAlertDisplayed = true
                        }
                        .confirmationDialog("Chcete smazat řidiče?", isPresented: $confirmationAlertDisplayed, titleVisibility: .visible) {
                            if driversVM.isLoading {
                                ProgressView()
                                    .progressViewStyle(.circular)
                                    .onDisappear {
                                        if !driversVM.errorOccr {
                                            dismiss()
                                        }
                                    }
                            } else {
                                Button("Smazat", role: .destructive) {
                                    Task {
                                        await driversVM.remove(driver.username)
                                    }
                                }
                            }
                            Button("Zrušit", role: .cancel) {}
                        }
                    }
                }
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    if driversVM.isLoading {
                        ProgressView()
                            .progressViewStyle(.circular)
                            .onDisappear {
                                if !driversVM.errorOccr {
                                    dismiss()
                                }
                            }
                    } else {
                        Button {
                            Task {
                                await driversVM.update(driver)
                            }
                        } label: {
                            Text("Uložit")
                                .fontWeight(.bold)
                        }
                        .disabled(driver.username.isEmpty)
                        .disabled(driver.firstName.isEmpty)
                        .disabled(driver.surName.isEmpty)
                    }
                }
            }
            .navigationTitle(driver.firstName)
        }
    }
}

struct DriversView_Previews: PreviewProvider {
    static var previews: some View {
        DriversView()
            .environmentObject(DriversVM())
    }
}

struct AddDriverView_Previews: PreviewProvider {
    static var previews: some View {
        AddDriverView()
            .environmentObject(DriversVM())
    }
}

struct EditDriverView_Previews: PreviewProvider {
    static var previews: some View {
        EditDriverView(driver: Driver(username: "rakovami", firstName: "Michaela", surName: "Raková"))
            .environmentObject(DriversVM())
    }
}
