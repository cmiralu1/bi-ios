//
//  TabiApp.swift
//  Tabi
//
//  Created by Lukáš Cmíral on 05.02.2023.
//

import SwiftUI

@main
struct TabiApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
