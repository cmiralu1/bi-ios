//
//  ContentView.swift
//  Tabi
//
//  Created by Lukáš Cmíral on 05.02.2023.
//

import SwiftUI

//var serverUrl = "http://192.168.0.100:8475"
//var serverUrl = "http://88.101.163.220:8475"
var serverUrl = "http://roo.tplinkdns.com:8475"

struct ContentView: View {
    @StateObject private var driversVM = DriversVM()
    @StateObject private var tripsVM = TripsVM()
    @StateObject private var expensesVM = ExpensesVM()
    @State var expenseSummary = ExpenseSummary()
    
    var body: some View {
        TabView {
            SummaryView(expenseSummary: $expenseSummary)
                .environmentObject(driversVM)
                .environmentObject(tripsVM)
                .environmentObject(expensesVM)
                .tabItem {
                    Label("Sourhn", systemImage: "house")
                }
            DriversView()
                .environmentObject(driversVM)
                .tabItem {
                    Label("Řidiči", systemImage: "person.2")
                }
            TripsView()
                .environmentObject(driversVM)
                .environmentObject(tripsVM)
                .tabItem {
                    Label("Jízdy", systemImage: "map")
                }
            ExpensesView()
                .environmentObject(expensesVM)
                .tabItem {
                    Label("Výdaje", systemImage: "creditcard")
                }
        }
        .onAppear {
            Task {
                await driversVM.fetchDrivers(triggerAlert: true) //show Cant connect to server alert on launch screen
                await tripsVM.fetchTrips(triggerAlert: false) //do not show Cant connect to server alert on other screens
                await expensesVM.fetchExpenses(triggerAlert: false)
                expenseSummary = expensesVM.calculateExpenses()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .preferredColorScheme(.light)
        ContentView()
            .preferredColorScheme(.dark)
    }
}
