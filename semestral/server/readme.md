# Useful commands:

**arm64** image

`docker build -t elukasino/ios-car-server .`

`docker push elukasino/ios-car-server:latest  `

or **amd64** image

`docker buildx create --name mybuilder --driver docker-container --bootstrap`

`docker buildx use mybuilder`

`docker buildx build --load --platform linux/amd64 -t elukasino/ios-car-server .`

`docker push elukasino/ios-car-server:latest  `
