package cz.cvut.fit.ios.car_trips.dao;

import cz.cvut.fit.ios.car_trips.domain.Cost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CostJpaRepository extends JpaRepository<Cost, Integer> {
}
