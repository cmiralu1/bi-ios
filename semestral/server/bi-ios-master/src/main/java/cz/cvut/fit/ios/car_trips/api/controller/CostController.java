package cz.cvut.fit.ios.car_trips.api.controller;

import cz.cvut.fit.ios.car_trips.api.converter.CostConverter;
import cz.cvut.fit.ios.car_trips.api.dto.CostDTO;
import cz.cvut.fit.ios.car_trips.business.CostService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.NoSuchElementException;

@RestController
public class CostController {
    private final CostService costService;

    CostController(CostService costService) {
        this.costService = costService;
    }

    @GetMapping("/costs")
    Collection<CostDTO> readAll() {
        return CostConverter.fromModels(costService.readAll());
    }

    @GetMapping("/costs/{id}")
    CostDTO readById(@PathVariable int id) {
        if(costService.readById(id).isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        else
            return CostConverter.fromModel(costService.readById(id).get());
    }

    @PostMapping("/costs")
    /*CostDTO*/ void create(@RequestBody CostDTO costDTO) {
        try {
            costService.create(CostConverter.toModel(costDTO));
            //return readById(costDTO.getID);
        }
        catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    @PutMapping("/costs/{id}")
    CostDTO update(@PathVariable int id, @RequestBody CostDTO costDTO) {
        if(costDTO.getID() != id)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "IDs do not match");
        try {
            costService.update(CostConverter.toModel(costDTO));
        }
        catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return readById(costDTO.getID());
    }

    @DeleteMapping("/costs/{id}")
    void delete(@PathVariable int id) {
        try {
            costService.deleteById(id);
        }
        catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/costs")
    void delete() {
        costService.deleteAll();
    }
}
