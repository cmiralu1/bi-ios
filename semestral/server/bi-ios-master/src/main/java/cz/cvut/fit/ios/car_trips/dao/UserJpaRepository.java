package cz.cvut.fit.ios.car_trips.dao;

import cz.cvut.fit.ios.car_trips.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserJpaRepository extends JpaRepository<User, String> {
}
