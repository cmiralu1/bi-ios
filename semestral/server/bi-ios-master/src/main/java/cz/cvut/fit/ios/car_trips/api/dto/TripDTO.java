package cz.cvut.fit.ios.car_trips.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class TripDTO {
    public String startDateTime_ID;
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy@HH:mm:ss")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "d. M. yyyy HH:mm:ss")
    private LocalDateTime startDateTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "d. M. yyyy HH:mm:ss")
    private LocalDateTime destDateTime;
    private String startAddr;
    private String destAddr;
    private double km; //todo maybe double
//    public Set<String> userUsernames = new HashSet<>();
    public String userUsername;

    public TripDTO() {}

    public TripDTO(String startDateTime_ID, LocalDateTime startDateTime, LocalDateTime destDateTime, String startAddr, String destAddr, double km, String userUsername) {
        this.startDateTime_ID = Objects.requireNonNull(startDateTime_ID);
        this.startDateTime = startDateTime;
        this.destDateTime = destDateTime;
        this.startAddr = startAddr;
        this.destAddr = destAddr;
        this.km = km;
        this.userUsername = userUsername;
    }

    public TripDTO(String startDateTime_ID, LocalDateTime startDateTime, LocalDateTime destDateTime, String startAddr, String destAddr, double km) {
        this.startDateTime_ID = Objects.requireNonNull(startDateTime_ID);
        this.startDateTime = startDateTime;
        this.destDateTime = destDateTime;
        this.startAddr = startAddr;
        this.destAddr = destAddr;
        this.km = km;
        this.userUsername = null;
    }

    public String getStartDateTime_ID() {
        return startDateTime_ID;
    }

    public void setStartDateTime_ID(String startDateTime_ID) {
        this.startDateTime_ID = startDateTime_ID;
    }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public LocalDateTime getDestDateTime() {
        return destDateTime;
    }

    public void setDestDateTime(LocalDateTime destDateTime) {
        this.destDateTime = destDateTime;
    }

    public String getStartAddr() {
        return startAddr;
    }

    public void setStartAddr(String startAddr) {
        this.startAddr = startAddr;
    }

    public String getDestAddr() {
        return destAddr;
    }

    public void setDestAddr(String destAddr) {
        this.destAddr = destAddr;
    }

    public double getKm() {
        return km;
    }

    public void setKm(double km) {
        this.km = km;
    }

    public String getUserUsername() {
        return userUsername;
    }

    public void setUserUsername(String userUsername) {
        this.userUsername = userUsername;
    }
}
