package cz.cvut.fit.ios.car_trips.domain;

import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Cost implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ID;
    private Integer czk;
    private CostType type;
    @Nullable
    private String description;

    public Cost() {}

    public Cost(Integer ID, Integer czk, CostType type, @Nullable String description) {
        this.ID = ID;
        this.czk = czk;
        this.type = type;
        this.description = description;
    }

    public Cost(Integer czk, CostType type, @Nullable String description) {
        this.czk = czk;
        this.type = type;
        this.description = description;
    }

    public Cost(Integer czk, CostType type) {
        this.czk = czk;
        this.type = type;
        this.description = null;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public Integer getCzk() {
        return czk;
    }

    public void setCzk(Integer czk) {
        this.czk = czk;
    }

    public CostType getType() {
        return type;
    }

    public void setType(CostType type) {
        this.type = type;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cost cost = (Cost) o;
        return Objects.equals(ID, cost.ID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID);
    }

    @Override
    public String toString() {
        return "Cost{" +
                "ID=" + ID +
                ", czk=" + czk +
                ", type=" + type +
                ", description='" + description + '\'' +
                '}';
    }
}
