package cz.cvut.fit.ios.car_trips.business;

import cz.cvut.fit.ios.car_trips.dao.TripJpaRepository;
import cz.cvut.fit.ios.car_trips.domain.Trip;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Objects;

@Component
public class TripService extends AbstractCRUDService<String, Trip, TripJpaRepository> {
    private final UserService userService;

    public TripService(TripJpaRepository repository, UserService userService) {
        super(repository);
        this.userService = userService;
    }

    @Override
    public boolean exists(Trip entity) {
        return repository.existsById(entity.getStartDateTime_ID());
    }

//    public Set<Trip> findTripsTripMinutesIsGreaterThan(int tripMinutes) { //unused, example of semiautomatic query
//        return repository.findAllByTripMinutesIsGreaterThan(tripMinutes);
//    }

    public void removeUserFromTrips(String usernameToDelete) {
        Collection<Trip> allTrips = readAll();
        for (Trip trip : allTrips) {
            if (trip.getUser() != null && Objects.equals(trip.getUser().getUsername(), usernameToDelete)) {
                trip.setUser(null);
            }
        }
    }
}
