package cz.cvut.fit.ios.car_trips.api.controller;

import cz.cvut.fit.ios.car_trips.api.converter.TripConverter;
import cz.cvut.fit.ios.car_trips.api.dto.TripDTO;
import cz.cvut.fit.ios.car_trips.business.TripService;
import cz.cvut.fit.ios.car_trips.business.UserService;
import cz.cvut.fit.ios.car_trips.domain.User;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.NoSuchElementException;

@RestController
public class TripController {
    private final TripService tripService;
    private final UserService userService;

    TripController(TripService tripService, UserService userService) {
        this.tripService = tripService;
        this.userService = userService;
    }

    @GetMapping("/trips")
    Collection<TripDTO> readAll() {
        try {
            return TripConverter.fromModels(tripService.readAll());
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/trips/{id}")
    TripDTO readById(@PathVariable String id) {
        if(tripService.readById(id).isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        else
            return TripConverter.fromModel(tripService.readById(id).get());
    }

    @PostMapping("/trips")
    TripDTO create(@RequestBody TripDTO tripDTO) {
        User user = null;
        if (tripDTO.getUserUsername() != null) {
            if (userService.readById(tripDTO.getUserUsername()).isEmpty())
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
            user = userService.readById(tripDTO.getUserUsername()).get();
        }
        try {
            tripService.create(TripConverter.toModel(tripDTO, user));
            return readById(tripDTO.getStartDateTime_ID());
        }
        catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

//    @PostMapping("/trips")
//    TripDTO create(@RequestBody TripDTO tripDTO) {
//        Set<User> users = new HashSet<>();
//        for(String username : tripDTO.userUsernames) {
//            if (userService.readById(username).isEmpty())
//                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
//            users.add(userService.readById(username).get());
//        }
//        if (carService.readById(tripDTO.getCarRegistration()).isEmpty())
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Car not found");
//        try {
//            tripService.create(TripConverter.toModel(tripDTO, carService.readById(tripDTO.getCarRegistration()).get(), users));
//            return readById(tripDTO.getTripId());
//        }
//        catch (IllegalArgumentException e) {
//            throw new ResponseStatusException(HttpStatus.CONFLICT);
//        }
//    }

    @PutMapping("/trips/{id}")
    TripDTO update(@PathVariable String id, @RequestBody TripDTO tripDTO) {
        if(!tripDTO.getStartDateTime_ID().equals(id))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Trip IDs do not match");

        User user = null;
        if (tripDTO.getUserUsername() != null) {
            if (userService.readById(tripDTO.getUserUsername()).isEmpty())
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
            user = userService.readById(tripDTO.getUserUsername()).get();
        }

        try {
            tripService.update(TripConverter.toModel(tripDTO, user));
        }
        catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return readById(tripDTO.getStartDateTime_ID());
    }

//    @PutMapping("/trips/{id}")
//    TripDTO update(@PathVariable Integer id, @RequestBody TripDTO tripDTO) {
//        if(!tripDTO.getTripId().equals(id))
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Trip IDs do not match");
//        Set<User> users = new HashSet<>();
//        for(String username : tripDTO.userUsernames) {
//            if (userService.readById(username).isEmpty())
//                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
//            users.add(userService.readById(username).get());
//        }
//        if (carService.readById(tripDTO.getCarRegistration()).isEmpty())
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Car not found");
//        try {
//            tripService.update(TripConverter.toModel(tripDTO, carService.readById(tripDTO.getCarRegistration()).get(), users));
//        }
//        catch (NoSuchElementException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
//        }
//        return readById(tripDTO.getTripId());
//    }

    @DeleteMapping("/trips/{id}")
    void delete(@PathVariable String id) {
        try {
            tripService.deleteById(id);
        }
        catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/trips")
    void delete() {
        tripService.deleteAll();
    }
}
