package cz.cvut.fit.ios.car_trips.business;

import cz.cvut.fit.ios.car_trips.domain.User;
import cz.cvut.fit.ios.car_trips.dao.UserJpaRepository;
import org.springframework.stereotype.Component;

@Component
public class UserService extends AbstractCRUDService<String, User, UserJpaRepository> {
    public UserService(UserJpaRepository repository) {
        super(repository);
    }

    @Override
    public boolean exists(User entity) {
        return repository.existsById(entity.getUsername());
    }
}