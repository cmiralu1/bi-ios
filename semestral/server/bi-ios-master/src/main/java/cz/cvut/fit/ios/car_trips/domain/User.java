package cz.cvut.fit.ios.car_trips.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

@Entity
public class User implements Serializable {
    @Id
    private String username;
    private String firstName;
    private String surName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Collection<Trip> myTrips;

    public User() {}

    public User(String username, String firstName, String surName) {
        this.username = username;
        this.firstName = firstName;
        this.surName = surName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public Collection<Trip> getMyTrips() {
        return myTrips;
    }

    public void setMyTrips(Collection<Trip> myTrips) {
        this.myTrips = myTrips;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return username.equals(user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", surName='" + surName + '\'' +
                '}';
    }
}
