package cz.cvut.fit.ios.car_trips.domain;

public enum CostType {
    FUEL, TIRES, SERVICE, OTHER
}
