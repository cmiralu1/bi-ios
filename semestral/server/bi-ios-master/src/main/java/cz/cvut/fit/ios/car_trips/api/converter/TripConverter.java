package cz.cvut.fit.ios.car_trips.api.converter;

import cz.cvut.fit.ios.car_trips.api.dto.TripDTO;
import cz.cvut.fit.ios.car_trips.domain.Trip;
import cz.cvut.fit.ios.car_trips.domain.User;

import java.util.ArrayList;
import java.util.Collection;

public class TripConverter {

    public static Trip toModel(TripDTO tripDTO, User user) {
        return new Trip(tripDTO.getStartDateTime_ID(), tripDTO.getStartDateTime(), tripDTO.getDestDateTime(), tripDTO.getStartAddr(), tripDTO.getDestAddr(), tripDTO.getKm(), user);
    }

//    public static Trip toModel(TripDTO tripDTO, Car car, Set<User> users) {
//        return new Trip(tripDTO.getTripId(), tripDTO.getTripMinutes(), tripDTO.getTripDateTime(), car, users);
//    }

    public static TripDTO fromModel(Trip trip) {
        if (trip.getUser() != null) {
            return new TripDTO(trip.getStartDateTime_ID(), trip.getStartDateTime(), trip.getDestDateTime(), trip.getStartAddr(), trip.getDestAddr(), trip.getKm(), trip.getUser().getUsername());
        }
        else
            return new TripDTO(trip.getStartDateTime_ID(), trip.getStartDateTime(), trip.getDestDateTime(), trip.getStartAddr(), trip.getDestAddr(), trip.getKm());
    }

//    public static TripDTO fromModel(Trip trip) {
//        Set<String> userUsernames = new HashSet<>();
//        for(User user : trip.getUsers()) {
//            userUsernames.add(user.getUsername());
//        }
//        if (trip.getCar() == null)
//            return new TripDTO(trip.getTripId(), trip.getTripMinutes(), trip.getTripDateTime(), "null", userUsernames);
//        return new TripDTO(trip.getTripId(), trip.getTripMinutes(), trip.getTripDateTime(), trip.getCar().getRegistration(), userUsernames);
//    }

//    public static Collection<Trip> toModels(Collection<TripDTO> tripDTOs) { //for future use
//        return tripDTOs.stream().map(TripConverter::toModel).toList();
//    }

//    public static Collection<Trip> toModels(Collection<TripDTO> tripDTOs) {
//        Collection<Trip> trips = new ArrayList<>();
//        tripDTOs.forEach((f) -> trips.add(toModel(f)));
//        return trips;
//    }

//    public static Collection<TripDTO> fromModels(Collection<Trip> trips) { //same functionality as fromModels below
//        return trips.stream().map(TripConverter::fromModel).toList();
//    }

    public static Collection<TripDTO> fromModels(Collection<Trip> trips) {
        Collection<TripDTO> tripDTOs = new ArrayList<>();
        trips.forEach((f) -> tripDTOs.add(fromModel(f)));
        return tripDTOs;
    }
}
