package cz.cvut.fit.ios.car_trips.api.converter;

import cz.cvut.fit.ios.car_trips.api.dto.CostDTO;
import cz.cvut.fit.ios.car_trips.domain.Cost;
import cz.cvut.fit.ios.car_trips.domain.CostType;

import java.util.Collection;

public class CostConverter {

    public static Cost toModel(CostDTO costDTO) {
        String typeRaw = costDTO.getType(); //FUEL, TIRES, SERVICE, OTHER
        String type = "";
        if (typeRaw != null)
            type = typeRaw.toLowerCase();
        if (costDTO.getID() == -1) {
            return switch (type) {
                case "fuel" -> new Cost(costDTO.getCzk(), CostType.FUEL, costDTO.getDescription());
                case "tires", "tire" -> new Cost(costDTO.getCzk(), CostType.TIRES, costDTO.getDescription());
                case "service" -> new Cost(costDTO.getCzk(), CostType.SERVICE, costDTO.getDescription());
                default -> new Cost(costDTO.getCzk(), CostType.OTHER, costDTO.getDescription());
            };
        }
        else {
            return switch (type) {
                case "fuel" -> new Cost(costDTO.getID(), costDTO.getCzk(), CostType.FUEL, costDTO.getDescription());
                case "tires", "tire" -> new Cost(costDTO.getID(), costDTO.getCzk(), CostType.TIRES, costDTO.getDescription());
                case "service" -> new Cost(costDTO.getID(), costDTO.getCzk(), CostType.SERVICE, costDTO.getDescription());
                default -> new Cost(costDTO.getID(), costDTO.getCzk(), CostType.OTHER, costDTO.getDescription());
            };
        }

    }

    public static CostDTO fromModel(Cost cost) {
        return switch (cost.getType()) {
            case FUEL -> new CostDTO(cost.getID(), cost.getCzk(), "fuel", cost.getDescription());
            case TIRES -> new CostDTO(cost.getID(), cost.getCzk(), "tires", cost.getDescription());
            case SERVICE -> new CostDTO(cost.getID(), cost.getCzk(), "service", cost.getDescription());
            default -> new CostDTO(cost.getID(), cost.getCzk(), "other", cost.getDescription());
        };
    }

    public static Collection<CostDTO> fromModels(Collection<Cost> costs) {
        return costs.stream().map(CostConverter::fromModel).toList();
    }
}
