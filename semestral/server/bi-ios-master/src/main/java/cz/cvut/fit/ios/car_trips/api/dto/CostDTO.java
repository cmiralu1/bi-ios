package cz.cvut.fit.ios.car_trips.api.dto;

import org.springframework.lang.Nullable;

public class CostDTO {

    public int ID; //-1 == ID not set yet (depends on database)
    private int czk;
    private String type; //FUEL, TIRES, SERVICE, OTHER
    private String description;
    public CostDTO() {}

    public CostDTO(int ID, int czk, String type, String description) {
        this.ID = ID; //todo
        this.czk = czk;
        this.type = type;
        this.description = description;
    }

    public CostDTO(int czk, String type, String description) { //todo pozor - po prichodu DTO bez ID se tento konstruktor z nejakeho duvodu nepouzije a vytvori se automaticky ID 0
        this.ID = -1; //ID not set yet
        this.czk = czk;
        this.type = type;
        this.description = description;
    }

    public int getID() {return ID;}

    public void setID(int ID) {this.ID = ID;}

    public int getCzk() {
        return czk;
    }

    public void setCzk(int czk) {
        this.czk = czk;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
