package cz.cvut.fit.ios.car_trips.api.converter;

import cz.cvut.fit.ios.car_trips.api.dto.UserDTO;
import cz.cvut.fit.ios.car_trips.domain.User;

import java.util.Collection;

public class UserConverter {

    public static User toModel(UserDTO userDTO) {
        return new User(userDTO.getUsername(), userDTO.getFirstName(), userDTO.getSurName());
    }

    public static UserDTO fromModel(User user) {
        return new UserDTO(user.getUsername(), user.getFirstName(), user.getSurName());
    }

    public static Collection<UserDTO> fromModels(Collection<User> users) {
        return users.stream().map(UserConverter::fromModel).toList();
    }
}
