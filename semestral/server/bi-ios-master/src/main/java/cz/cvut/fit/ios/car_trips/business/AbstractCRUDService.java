package cz.cvut.fit.ios.car_trips.business;

import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Common superclass for business logic of all entities supporting operations Create, Read, Update, Delete.
 *
 * @param <K> Type of (primary) key.
 * @param <E> Type of entity
 */
public abstract class AbstractCRUDService<K, E, REPOSITORY extends JpaRepository<E, K>> {
    protected final REPOSITORY repository;
    protected AbstractCRUDService(REPOSITORY repository) {
        this.repository = repository;
    }

    public abstract boolean exists(E entity);

    public Collection<E> readAll() throws NoSuchElementException {
        try {
            return repository.findAll();
        } catch (Throwable e) {
            throw new NoSuchElementException("Entity not found");
        }
    }

    public Optional<E> readById(K id) {
        return repository.findById(id);
    }

    @Transactional
    public void create(E entity) throws IllegalArgumentException{
        if (exists(entity))
            throw new IllegalArgumentException("Entity already exists");
        repository.save(entity);
    }

    @Transactional
    public void update(E entity) throws NoSuchElementException {
        if(exists(entity))
            repository.save(entity);
        else
            throw new NoSuchElementException("Entity not found");
    }

    @Transactional
    public void deleteById(K id) throws NoSuchElementException {
        if (repository.findById(id).isPresent())
            repository.deleteById(id);
        else
            throw new NoSuchElementException("Entity not found");
    }

    @Transactional
    public void deleteAll() {
        repository.deleteAll();
    }
}