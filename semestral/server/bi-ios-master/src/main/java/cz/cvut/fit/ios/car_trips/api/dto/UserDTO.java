package cz.cvut.fit.ios.car_trips.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;
import java.util.Objects;

public class UserDTO {
    public String username;
    private String firstName;
    private String surName;

    public UserDTO() {}

    public UserDTO(String username, String firstName, String surName) {
        this.username = username;
        this.firstName = firstName;
        this.surName = surName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }
}
