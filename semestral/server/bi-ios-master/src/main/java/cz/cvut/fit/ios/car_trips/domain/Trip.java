package cz.cvut.fit.ios.car_trips.domain;

import org.springframework.lang.Nullable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
public class Trip implements Serializable {
    @Id
    private String startDateTime_ID;
    //@GeneratedValue(strategy = GenerationType.IDENTITY)

    private LocalDateTime startDateTime;
    private LocalDateTime destDateTime;
    private String startAddr;
    private String destAddr;
    private Double km;
    @ManyToOne
    @Nullable
    private User user;

    public Trip() {}

    public Trip(String startDateTime_ID, LocalDateTime startDateTime, LocalDateTime destDateTime, String startAddr, String destAddr, Double km, @Nullable User user) {
        this.startDateTime_ID = startDateTime_ID;
        this.startDateTime = startDateTime;
        this.destDateTime = destDateTime;
        this.startAddr = startAddr;
        this.destAddr = destAddr;
        this.km = km;
        this.user = user;
    }

    public Trip(String startDateTime_ID, LocalDateTime startDateTime, LocalDateTime destDateTime, String startAddr, String destAddr, Double km) {
        this.startDateTime_ID = startDateTime_ID;
        this.startDateTime = startDateTime;
        this.destDateTime = destDateTime;
        this.startAddr = startAddr;
        this.destAddr = destAddr;
        this.km = km;
        this.user = null;
    }

    public String getStartDateTime_ID() {
        return startDateTime_ID;
    }

    public void setStartDateTime_ID(String startDateTime_ID) {
        this.startDateTime_ID = startDateTime_ID;
    }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public LocalDateTime getDestDateTime() {
        return destDateTime;
    }

    public void setDestDateTime(LocalDateTime destDateTime) {
        this.destDateTime = destDateTime;
    }

    public String getStartAddr() {
        return startAddr;
    }

    public void setStartAddr(String startAddr) {
        this.startAddr = startAddr;
    }

    public String getDestAddr() {
        return destAddr;
    }

    public void setDestAddr(String destAddr) {
        this.destAddr = destAddr;
    }

    public Double getKm() {
        return km;
    }

    public void setKm(Double km) {
        this.km = km;
    }

    @Nullable
    public User getUser() {
        return user;
    }

    public void setUser(@Nullable User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trip trip = (Trip) o;
        return Objects.equals(startDateTime_ID, trip.startDateTime_ID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startDateTime_ID);
    }

    @Override
    public String toString() {
        return "Trip{" +
                "startDateTime_ID='" + startDateTime_ID + '\'' +
                ", startDateTime=" + startDateTime +
                ", destDateTime=" + destDateTime +
                ", startAddr='" + startAddr + '\'' +
                ", destAddr='" + destAddr + '\'' +
                ", km=" + km +
                ", user=" + user +
                '}';
    }
}