package cz.cvut.fit.ios.car_trips.business;

import cz.cvut.fit.ios.car_trips.dao.CostJpaRepository;
import cz.cvut.fit.ios.car_trips.domain.Cost;
import org.springframework.stereotype.Component;

@Component
public class CostService extends AbstractCRUDService<Integer, Cost, CostJpaRepository> {
    public CostService(CostJpaRepository repository) {
        super(repository);
    }

    @Override
    public boolean exists(Cost entity) {
        return repository.existsById(entity.getID());
    }
}