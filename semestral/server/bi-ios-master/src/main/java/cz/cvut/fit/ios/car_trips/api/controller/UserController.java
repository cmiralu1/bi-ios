package cz.cvut.fit.ios.car_trips.api.controller;

import cz.cvut.fit.ios.car_trips.api.dto.UserDTO;
import cz.cvut.fit.ios.car_trips.api.converter.UserConverter;
import cz.cvut.fit.ios.car_trips.business.TripService;
import cz.cvut.fit.ios.car_trips.business.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.NoSuchElementException;

@RestController
public class UserController {
    private final UserService userService;
    private final TripService tripService;

    UserController(UserService userService, TripService tripService) {
        this.userService = userService;
        this.tripService = tripService;
    }

    @GetMapping("/users")
    Collection<UserDTO> readAll() {
        return UserConverter.fromModels(userService.readAll());
    }

    @GetMapping("/users/{id}")
    UserDTO readById(@PathVariable String id) {
        if(userService.readById(id).isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        else
            return UserConverter.fromModel(userService.readById(id).get());
    }

    @PostMapping("/users")
    UserDTO create(@RequestBody UserDTO userDTO) {
        try {
            userService.create(UserConverter.toModel(userDTO));
            return readById(userDTO.getUsername());
        }
        catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    @PutMapping("/users/{id}")
    UserDTO update(@PathVariable String id, @RequestBody UserDTO userDTO) {
        if(!userDTO.getUsername().equals(id))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Usernames do not match");
        try {
            userService.update(UserConverter.toModel(userDTO));
        }
        catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return readById(userDTO.getUsername());
    }

    @DeleteMapping("/users/{id}")
    void delete(@PathVariable String id) {
        try {
            tripService.removeUserFromTrips(id);
            userService.deleteById(id);
        }
        catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
