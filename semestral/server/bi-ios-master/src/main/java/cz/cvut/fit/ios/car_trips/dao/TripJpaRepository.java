package cz.cvut.fit.ios.car_trips.dao;

import cz.cvut.fit.ios.car_trips.domain.Trip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TripJpaRepository extends JpaRepository<Trip, String> {
    //Set<Trip> findAllByTripMinutesIsGreaterThan(int tripMinutes); //unused, example of semiautomatic query
}
