# Semestrální práce BI-IOS - serverová část

Semestrální práce předmětu BI-IOS na téma: počitadlo ujetých kilometrů vozu a poměrná kalkulace ceny za palivo jednotlivým řidičům. Používány jsou entity `User`, `Cost` a `Trip`.

### Useful building commands:

**arm64** image

`docker build -t elukasino/ios-car-server .`

`docker push elukasino/ios-car-server:latest  `

or **amd64** image

`docker buildx create --name mybuilder --driver docker-container --bootstrap`

`docker buildx use mybuilder`

`docker buildx build --load --platform linux/amd64 -t elukasino/ios-car-server .`

`docker push elukasino/ios-car-server:latest  `

Vzorové REST požadavky připravené v pořadí shora dolu jsou k dispozici v souborech <nobr>`rest-examples-fill.http` a `rest-examples.http`.</nobr>