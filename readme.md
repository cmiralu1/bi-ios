# BI-IOS
## 1. úkol
Nahraný je první úkol BI-IOS, většina změn se nachází v souboru PostDetailView. Bonus na skrytí informací je obsažen.

## 2. úkol
Druhý úkol je nahraný. Většina změn se nachází v balíčku Post. Pro zobrazení nově přidaného příspěvku je nutné použít pull-to-refresh. Aplikace upozorňuje na chyby alertem a výpisem do konzole. Uživatelské jméno se vyplňuje zvlášt v sekci Profile.

## semestrální práce
V semestrální práci vytvářím aplikaci, která umí shromažďovat jízdy sdíleným domácím automobilem a ke každé jízdě lze přiřadit řidiče. Dále je do aplikace možné zaznamenat náklady spojené s vozidlem včetně paliva. Ve výsledku pak umí každému řidiči vypočítat, kolik musí zaplatit za provoz podle počtu ujetých kilometrů.


Práce má dvě části: aplikaci a server. Aplikace se pomocí API připojuje na server, který ukládá data do databáze. Díky tomu je možné, aby aplikaci mělo více lidí na vlastních telefonech a každý může mít přehled o svých nákladech.


Serverová část momentálně běží na domácím NASu v podobě Docker containeru. Připojit se lze přes adresu `http://roo.tplinkdns.com:8475` nebo při spuštění lokálně na portu 8475. Užívaná adresa se specifikuje v kódu aplikace (ContentView.swift). Docker image je ke stažení na adrese https://hub.docker.com/repository/docker/elukasino/ios-car-server/general - k dispozici je verze v5 pro architekturu amd64 a arm64.


Aplikace importuje jízdy z csv souboru, který vygeneruje aplikace výrobce vozidla. Ukázkové testovací csv soubory jsou přiloženy v příslušném adresáři.


Aplikace využívá externí balíčky:
SwiftCSV: https://github.com/swiftcsv/SwiftCSV
SwiftUICharts: https://github.com/AppPear/ChartView


cmiralu1, Lukáš Cmíral
